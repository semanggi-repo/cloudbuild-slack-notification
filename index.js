const IncomingWebhook = require('@slack/client').IncomingWebhook;
const SLACK_WEBHOOK_URL = process.env.SLACK_WEBHOOK_URL;

const webhook = new IncomingWebhook(SLACK_WEBHOOK_URL);

// eventToBuild transforms pubsub event message to a build object.
const eventToBuild = (data) => {
  return JSON.parse(new Buffer(data, 'base64').toString());
}

const color = (build) => {
  switch (build.status) {
    case 'SUCCESS':
      return '#36A64F';
    case 'QUEUED':
      return '#B0B0B0';
    case 'WORKING':
      return '#7898FC';
    default:
      return '#B94A4B'
  }
}

const duration = (milli) => {
  const seconds = Math.floor((milli / 1000) % 60);
  const minutes = (Math.floor((milli / (60 * 1000)) % 60));
  const dur = (minutes) ? `${minutes} min ${seconds} sec` : `${seconds} sec`;
  return dur;
}

const footer = (build) => (['WORKING', 'QUEUED', 'STATUS_UNKNOWN'].includes(build.status)) ?
                             null :
                             duration(new Date(build.finishTime) - new Date(build.startTime));

// createSlackMessage creates a message from a build object.
const createSlackMessage = (build) => {
  let message = {
    text: null,
    attachments: [{
      color: color(build),
      title: `[${build.status.toUpperCase()}] | ${build.source.repoSource.repoName.split('_')[2]}`,
      text: `Branch: ${build.source.repoSource.branchName}`,
      footer: footer(build)
 }]
}
  return message
}

exports.subscribeSlack = (event, callback) => {
  try {
    const build = eventToBuild(event.data);

  const status = ['SUCCESS', 'FAILURE', 'INTERNAL_ERROR', 'TIMEOUT'];
  if (status.indexOf(build.status) === -1) {
    return callback();
  }

  // Send message to Slack.
  if (build.steps[0].name !== 'gcr.io/cloud-builders/gcs-fetcher') {
    const message = createSlackMessage(build);
    console.log(JSON.stringify(message));
    
    webhook.send(message, callback);
  }
  } catch (error) {
   console.error(error);
    
  }
};